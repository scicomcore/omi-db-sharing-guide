# Database JSONs

OMI-DB is centred around two key databases:

1. **NBSS**: National Breast Screening Service database
    - Exists at each of the remote sites where images are collected
    - The automated collection system collects de-identified information from the NBSS describing the diagnosis and screening history of the cases collected

2. **ImageDB**:
    - Stores attributes extracted from the collected DICOM images
    - Establishes necessary links between the clients in NBSS and the corresponding images references by ImageDB
    - Where available, stores lesion information and associated ground truth marks as annotated by expert radiologists using bespoke medical imaging software

Various fields have been extracted and appropriately re-pseudonymised (original
pseudonyms replaced with new pseudonyms for sharing) from the two databases and
processed as JSON metadata to accompany the shared images.  The basic structure
of the database is captured by Table \ref{tab:records}, which shows the main types of
records referenced throughout the metadata and DICOM images themselves (refer
to the tags). The row order of the records reflects the hierarchical nature of
the database, which roughly follows the nested structure of the DICOM object
model. The second column 'JSON Reference' shows the JSON keys that refer to
record identifiers (either directly using the ID value or indirectly using the
ID key).


\begin{table}[]
\begin{tabularx}{\linewidth}{l l l X}
Record Type & \begin{tabular}[t]{@{}l@{}}JSON Reference\\ (\textbf{NBSS} / ImageDB)\end{tabular}                                                      & DICOM Tag                                                                 & Description                                                                                                                                                                                                                                                                                            \\
\hline
\hline
Client      & \begin{tabular}[t]{@{}l@{}}\textbf{ClientID}\\ ClientID\end{tabular}                                                                    & \begin{tabular}[t]{@{}l@{}}PatientID\\ (0010,0020)\end{tabular}           & A patient identifier                                                                                                                                                                                                                                                                                   \\
\hline
Episode     & \begin{tabular}[t]{@{}l@{}}\textbf{Episode ID value}\\ EpisodeID\end{tabular}                                                           & -                                                                         & A set of medical procedures to treat or diagnose a clinical condition. In the case of triennial screening with no further assessments, each episode will comprise a single screening event.                                                                                                            \\
\hline
Event       & \bf\begin{tabular}[t]{@{}l@{}}SCREENING\\ ASSESSMENT\\ BIOPSYWIDE\\ BIOPSYFINE\\ SURGERY\\ CLINICAL\\ INTERVALCANCER\end{tabular} & -                                                                         & A single medical procedure or examination, e.g. screening, surgery, or biopsy.                                                                                                                                                                                                                         \\
\hline
Study       & \begin{tabular}[t]{@{}l@{}}\textbf{StudyList} \\ Study IUID value \end{tabular}                                                                                                      & \begin{tabular}[t]{@{}l@{}}Study Instance UID\\ (0020,000D)\end{tabular}  & An imaging examination comprising a number of series. All images from these examinations will have the same Study IUID.                                                                                                                                                                                \\
\hline
Series      & Series IUID value                                                                                                              & \begin{tabular}[t]{@{}l@{}}Series Instance UID\\ (0020,000E)\end{tabular} & Collection of images taken during one examination by one modality, for a given position of the patient on the acquisition device. For mammograms, each image is (typically) associated with one unique Series IUID, and is therefore omitted in the OMI-DB tree hierarchy (there is no Series folder). \\
\hline
Image       & SOP IUID value                                                                                                                 & \begin{tabular}[t]{@{}l@{}}SOP Instance UID\\ (0008,0018)\end{tabular}    & A DICOM image which may be annotated. Both processed and unprocessed images are shipped with OMI-DB.                                                                                                                                                                                                   \\
\hline
Mark        & \begin{tabular}[t]{@{}l@{}}MarkID\\ MarkID value\end{tabular}                                                                  &                                                                           & A ground-truth region of interest made by an expert radiologist. A mark includes the region of interest and descriptions characterising the lesion. An image can have multiple marks for one or many lesions.

\end{tabularx}
\caption{Overview of the key record types used throughout OMI-DB. Bold references shown in the second column correspond to those in the NBSS JSON; non-bold refer to those in the ImageDB JSON.}
\label{tab:records}
\end{table}

\newpage

## NBSS JSON (`nbss_demdX.json`)

An NBSS JSON contains one object for each episode (keyed by the episode ID),
and two strings denoting the classification and client ID, respectively:

```json
{
	"9975": {...},
	"9979": {...},
	"9984": {...},
	"9999": {...},
	"Classification": "M",
	"ClientID": "demd2431"
}
```


Each episode object contains various properties summarising the episode,
provides detailed information on all events and lesions (if any), in addition
to references to associated studies:

\footnotesize

```json
"9975": {
	"AccessionID": "CRY4881106, CRY169095067, CRY1211442154, CRY2372635513",
	"EpisodeAction": "FP",
	"EpisodeClosedDate": "2014-05-23",
	"EpisodeIsClosed": "Y",
	"EpisodeOpenedDate": "2013-02-26",
	"EpisodeType": "R",
	"PatientAge": 72,
	"StudyList":
	"1.2.826.0.1.3680043.9.3218.1.1.3866541.1754.1511400095328.1727.0,...,
    1.2.826.0.1.3680043.9.3218.1.1.3866541.1754.1511400095328.1669.0", // A single string

	"ASSESSMENT": {...},
	"BIOPSYWIDE": {...},
	"CLINICAL": {...},
	"INTERVALCANCER": {...},
	"LESION": {...},
	"SCREENING": {...},
	"SURGERY": {...}
}
```

\normalsize

Note that the existence of event and lesion keys depends on episode and client,
e.g. `nbss_demdX["Y"]["SURGERY"]` won't exist if client _demdX_ did not have
surgery in episode _Y_. Further, lesions in NBSS are not directly linked to
those in ImageDB. Descriptions of the top-level properties and episode
properties are provided by Table \ref{tab:nbss_props}. Refer to the [NBSS User Guide](https://bitbucket.org/scicomcore/omi-db-sharing-guide/raw/d9e023e39760b12decec67f6407beee91cb82ac2/pdfs/crystal_nbss_guide.pdf) for descriptions of the fields contained within the event and lesion objects.

\begin{table}[]
\begin{tabularx}{\linewidth}{l X}
\bf Property                             & \bf Description                                                                                                                                                                                                                                                                                                                                                                                                                                          \\
\hline
\hline
Classification                       & \begin{tabular}[t]{@{}l@{}}\textbf{N}: Normal\\ \textbf{M}: Malignant\\ \textbf{B}: Benign\\ \textbf{CI}: Interval cancer\end{tabular}                                                                                                                                                                                                                                                                                                                 \\
\hline
ClientID                             & \begin{tabular}[t]{@{}l@{}}Unique identifier: \\ demd followed by a numeric\end{tabular}                                                                                                                                                                                                                                                                                                                                                             \\
\Xhline{1.5pt}
AccessionID                          & A string of comma separated accession IDs linking NBSS appointment with imaging                                                                                                                                                                                                                                                                                                                                                                      \\
\hline
EpisodeAction                        & \begin{tabular}[t]{@{}l@{}}\textbf{EC}: Early Recall for Clinic\\ \textbf{ES}: Early Recall for Screening\\ \textbf{FN}: Fine Needle Aspiration\\ \textbf{FP}: Follow-up (Post-treatment)\\ \textbf{FV}: Further X-ray views\\ \textbf{IP}: Inpatient biopsy\\ \textbf{MT}: Medical Treatment\\ \textbf{NA}: Action from this procedure\\ \textbf{RC}: Review in clinic\\ \textbf{RF}: Referral to consultant/GP\\ \textbf{RR}: Routine recall for screening\\ \textbf{ST}: Surgical Treatment\\ \textbf{TR}: Repeat Film (technical)\\ \textbf{WB}: Wide Bore Needle\end{tabular} \\
\hline
EpisodeClosedDate                    & Date of episode closure: year-month-day                                                                                                                                                                                                                                                                                                                                                                                                              \\
\hline
EpisodeIsClosed                      & \begin{tabular}[t]{@{}l@{}}\textbf{Y}: Yes\\ \textbf{N}: No\end{tabular}                                                                                                                                                                                                                                                                                                                                                                                               \\
\hline
EpisodeOpenedDate                    & Date of episode opening: year-month-day                                                                                                                                                                                                                                                                                                                                                                                                              \\
\hline
EpisodeType                          & \begin{tabular}[t]{@{}l@{}}\textbf{C}: Call\\ \textbf{R}: Recall\end{tabular}                                                                                                                                                                                                                                                                                                                                                                                          \\
\hline
PatientAge                           & Patient age \textbf{at the time the NBSS database was queried}                                                                                                                                                                                                                                                                                                                                                                                                                            \\
\hline
StudyList                            & String of comma separated study IUIDs                                                                                                                                                                                                                                                                                                                                                                                                                \\
\hline
ASSESSMENT                           & Refer to ImagingDS (page 55) and ImagingProcedure (page 56) tables of \href{https://bitbucket.org/scicomcore/omi-db-sharing-guide/raw/d9e023e39760b12decec67f6407beee91cb82ac2/pdfs/crystal_nbss_guide.pdf}{NBSS User Guide} \\
\hline
BIOPSYWIDE                           & Refer to WbnDS (page 100)  and WbnProcedure (page 101) tables of \href{https://bitbucket.org/scicomcore/omi-db-sharing-guide/raw/d9e023e39760b12decec67f6407beee91cb82ac2/pdfs/crystal_nbss_guide.pdf<Paste>}{NBSS User Guide} \\
\hline
CLINICAL                             & Refer to ClinicalExamDS (page 25) and ClinicalExamProcedures table of \href{https://bitbucket.org/scicomcore/omi-db-sharing-guide/raw/d9e023e39760b12decec67f6407beee91cb82ac2/pdfs/crystal_nbss_guide.pdf}{NBSS User Guide} (page 26)                                                                                                                                                                                                                                                                                               \\
\hline
LESION                               & Refer to Lesion table (page 26) of \href{https://bitbucket.org/scicomcore/omi-db-sharing-guide/raw/d9e023e39760b12decec67f6407beee91cb82ac2/pdfs/crystal_nbss_guide.pdf}{NBSS User Guide}                                                                                                                                                                                                                                                                                                                                            \\
\hline
SCREENING                            & Refer to ScreeningDS (page 81) and ScreeningProcedure (page 84) tables of \href{https://bitbucket.org/scicomcore/omi-db-sharing-guide/raw/d9e023e39760b12decec67f6407beee91cb82ac2/pdfs/crystal_nbss_guide.pdf}{NBSS User Guide}. See Appendix~A for comments on the FilmReaderOpinion property of the ScreeningProcedure table.                                                                                                                                                                                                                                                                                                     \\
\hline
OTHERSCREENING                       & Contains information about one or more screening events conducted prior to the (repeated) screen as recorded in SCREENING. Typically occurs during a \emph{Technical Recall}. Child objects have the same schema as SCREENING. \\
\hline
SURGERY                              & Refer to SurgeryDS (page 93) and SurgeryProcedure (page 94) tables of \href{https://bitbucket.org/scicomcore/omi-db-sharing-guide/raw/d9e023e39760b12decec67f6407beee91cb82ac2/pdfs/crystal_nbss_guide.pdf}{NBSS User Guide}                                                                                                                                                                                                                                                                                                         \\
\hline
BIOPSYFINE                           & Refer to FnaDS (page 40) and FnaProdcure (page 42) tables of \href{https://bitbucket.org/scicomcore/omi-db-sharing-guide/raw/d9e023e39760b12decec67f6407beee91cb82ac2/pdfs/crystal_nbss_guide.pdf}{NBSS User Guide}                                                                                                                                                                                                                                                                                                                  \\
\hline
INTERVALCANCER                       & Refer to IntervalCancerSymptomatic (page 61) table of \href{https://bitbucket.org/scicomcore/omi-db-sharing-guide/src/master/pdfs/crystal_nbss_guide.pdf}{NBSS User Guide}
\end{tabularx}
\caption{Descriptions of the NBSS JSON properties, with episode properties listed below the bold horizontal line.}
\label{tab:nbss_props}
\end{table}

\newpage

## ImageDB JSON (`imagedb_demdX.json`)

The ImageDB metadata contains the co-ordinates and descriptors of any lesions
marked by expert radiologists. The image below shows the interface used to
characterise a marked lesion.

![Interface used to characterise a marked lesion.](./images/lesion_interface_scaled.png "lesion interface")

\newpage
The following artificial ImageDB JSON exemplifies a typical document:

\footnotesize

```json
{
	"ClientID": "demd243422",
	"Site": "stge",
	"Timestamp": 1552743769,
	"LESIONS": {"2165": {"LesionID": "2165"}},
	"STUDIES": {
    	"1.2.826.0.1.3680043.9.3218.1.1.3866541.1754.1511400095328.1702.0": {

        	"EpisodeID": "9977",
        	"StudyDate": "20120524",

        	"1.2.826.0.1.3680043.9.3218.1.1.3866541.1754.1511400095328.1703.0": {
            	"1.2.826.0.1.3680043.9.3218.1.1.3866541.1754.1511400095328.1705.0": null
        	},
        	"1.2.826.0.1.3680043.9.3218.1.1.3866541.1754.1511400095328.1709.0": {
            	"1.2.826.0.1.3680043.9.3218.1.1.3866541.1754.1511400095328.1711.0": {
                	"4015": {
                        "LinkedNBSSLesionNumber": "1",
                    	"ArchitecturalDistortion": null,
                    	"BenignClassification": null,
                    	"Conspicuity": "Obvious",
                    	"Dystrophic": null,
                    	"FatNecrosis": null,
                    	"FocalAsymmetry": "focal_asymmetry",
                    	"Height": "198",
                    	"LesionID": "2165",
                    	"MarkID": "4015",
                    	"Mass": 1,
                    	"MassClassification": "ill_defined",
                    	"MilkOfCalcium": null,
                    	"OtherBenignCluster": null,
                    	"PlasmaCellMastitis": null,
                    	"Skin": null,
                    	"SuspiciousCalcifications": null,
                    	"SutureCalcification": null,
                    	"Vascular": null,
                    	"Width": "261",
                    	"WithCalcification": null,
                    	"X1": "2508",
                    	"X2": "2769",
                    	"Y1": "1271",
                    	"Y2": "1469"
                	}
            	}
        	}
    	}
	}
    }
```

\normalsize

In general, the ImageDB JSON for a given client is only useful if it contains
annotated lesions, which can be identified by a non-empty object keyed by
`LESIONS`. If one simply wants to view the hierarchical structure of a client
or extract record identifiers, this document serves as a good starting point.

In the `demd243522` example above, it can be seen that there is a single lesion
with an ID of `2165`, mark and lesion information for which is nested under:

Episode ID: `9977`

Study IUID: `1.2.826.0.1.3680043.9.3218.1.1.3866541.1754.1511400095328.1702.0`

Series IUID: `1.2.826.0.1.3680043.9.3218.1.1.3866541.1754.1511400095328.1709.0`

SOP IUID: `1.2.826.0.1.3680043.9.3218.1.1.3866541.1754.1511400095328.1711.0`

A mark object is keyed by its ID (`4015` in this case) and contains properties
defining the mark and characterising the lesion. Note that empty or missing
values are replaced with `null`. Similarly, unmarked images, e.g.
`1.2.826.0.1.3680043.9.3218.1.1.3866541.1754.1511400095328.1705.0` in the
example, also receive `null`. Mark properties are described in Table
\ref{tab:mark_props}.

\begin{table}[h!]
\begin{center}
\begin{tabularx}{\linewidth}{l l}
\bf Property            & \bf Description                                                                           \\
\hline
\hline
LinkedNBSSLesionNumber & The NBSS lesion number corresponding to the marked lesion. \\ & Only unique within an episode. \\ \hline
ArchitecturalDistortion  & Indicates this mark is an Architectural Distortion*                            \\
\hline


BenignClassification     & \begin{tabular}[c]{@{}l@{}}Description of the benign feature: \\
\textbf{coarse\_or\_popcorn-like} \\
\textbf{lucent-centred} \\
\textbf{egg\_shell\_or\_rim} \\
\textbf{rod-like} \\
\textbf{round\_and\_punctate}
\end{tabular} \\

\hline

Conspicuity              & \begin{tabular}[c]{@{}l@{}}Indicates this marks conspicuity: \\
\textbf{Obvious} \\
\textbf{Subtle} \\
\textbf{Very\_subtle} \\
\textbf{Occult} \\
\textbf{not\_recalled}
\end{tabular} \\

\hline

Dystrophic               & Dystrophic* \\
\hline
FatNecrosis              & Fat Necrosis*                                                                          \\
\hline
FocalAsymmetry           & Indicates this mark is a Focal Asymmetry*                                              \\
\hline
Height                   & The height of the mark                                                                \\
\hline
LesionID                 & Lesion identifier                                                                     \\
\hline
MarkID                   & Mark identifier                                                                       \\
\hline
Mass                     & Indicates this mark is a Mass*                                                         \\
\hline

MassClassification       & \begin{tabular}[c]{@{}l@{}}Description of the mass border: \\
\textbf{well\_defined} \\
\textbf{ill\_defined} \\
\textbf{spiculated} \\
\textbf{other} \\
\textbf{unknown} \\
\textbf{lucent-centred} \\
\textbf{coarse\_or\_popcorn-like} \\
\textbf{egg\_shell\_or\_rim}
\end{tabular} \\

\hline

MilkOfCalcium            & Milk Of Calcium*                                                                    \\
\hline
OtherBenignCluster       & Other Benign Features*                                                                 \\
\hline
PlasmaCellMastitis       & Plasma Cell Mastitis*                                                                 \\
\hline
Skin                     & Benign Skin Feature*                                                                 \\
\hline
SuspiciousCalcifications & Indicates this mark is over Suspicious Calcifications*                                 \\
\hline
SutureCalcification      & Suture Calcification*                                \\
\hline
Vascular                 & Vascular Feature*                                                                      \\
\hline
Width                    & The width of the mark                                                                 \\
\hline
WithCalcification        & Indicates this mark has calcifications*                                                \\
\hline
X1                       & X1 coordinate                                                                         \\
\hline
X2                       & X2 coordinate                                                                         \\
\hline
Y1                       & Y1 coordinate                                                                         \\
\hline
Y2                       & Y2 coordinate

\end{tabularx}
\end{center}
\caption{
Descriptions of the mark properties given in the ImageDB JSON.  Where
applicable, property values are given in bold, though some values may be an
empty string if missing. Asterisks denote boolean properties with values
represented by either null/empty-string (false), or the name of the property
itself in lower-snake-case form or 1 (true).
}
\label{tab:mark_props}
\end{table}

As shown in the first row of the table, `LinkedNBSSLesionNumber` links an annotation in IMAGEDB to a lesion, listed by the `LESION` object and one or more events, of the NBSS, for the corresponding episode. Note that each number serves as a key in the NBSS JSON, and is unique _within_ an episode. This property is derived after extracting information from each database, and applying bespoke linking-logic based on the number of lesions and their status.
