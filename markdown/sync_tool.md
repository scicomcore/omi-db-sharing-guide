# The OMI-DB Sync Tool

The OMI-DB Sync Tool is a cross-platform desktop application that enables
researchers, academics and organisations to easily download subsets (depending
on the sharing agreement) of the core image dataset.  Prior to this
application, data sharing involved manually dividing a copy of the database
into several file archives which were then distributed to desktop clients using
[Dropbox shared
links](https://www.dropbox.com/help/files-folders/view-only-access). After
downloading these compressed archive files, recipients were then required to
perform file-integrity verification and subsequently compile the database
themselves. The goal of this software is to automate and simplify the sharing
process, without introducing data redundancy on the remote storage server.

When a user starts a sync, the tool looks up which resources (typically client
or study folders) the user has access to, connects to the central repository
stored on the Dropbox remote, and then downloads each resource to their local
machine. **If a resource already exists on the user's machine, the tool will
check the integrity of all JSON/DICOM files associated with that resource, and
determine if a new download is required** (though this stage can be bypassed to
speed up the initial transfer). Note that only JSON files are 'syncable', so if
the user deletes an image, they will need to [contact the support
team](mailto:rsch.scicom@nhs.net?subject=OMI-DB Sync Tool) should they wish to
restore it.

Important points:

- Any files that do not belong to the database will be deleted from the
  specified download directory.

- We recommend that the user specifies a new (empty) directory which has
    sufficient space for the entire database (see the step-by-step guide below).

- Because of the file-integrity verification that occurs after downloading each
  resource, **we do not recommend** transferring data directly to a mounted
  network drive, e.g. Azure Blob storage.

- The application must have read and write permission for both the
  download directory and the user's home folder.

- Correct synchronisation requires a local state file to be stored in
  the download directory: the same directory should be used for every sync.

- Running multiple syncs at the same time may lead to rate-limiting, connection
  dropouts and incomplete downloads: only have one instance of the app open.

- A log file is stored in the user's home directory. This is useful for
    following up on any sync issues that occurred when running the tool.

## Download

Use the links below to download the latest executable for your desktop machine
(note that the bundle does not need installing):

- Debian GNU/Linux 64-bit: [https://bit.ly/38b0cY1](https://bit.ly/38b0cY1)
- Linux Manjaro 64-bit: [https://bit.ly/3es03AF](https://bit.ly/3es03AF)
- Windows 64-bit: [https://bit.ly/3eFWtTH](https://bit.ly/3eFWtTH)
- macOS: [https://bit.ly/3l7veUs](https://bit.ly/3l7veUs)


## Step-by-step guide

1. The Sync Tool does not need to be installed, and should run from any location.
   Open the application and login by entering your email address and the application password that was sent to you.

    ![Login screen.](./images/login.png "Login screen")

    The status bar at the bottom of the window displays some useful information
    about current activity:

    ![Status bar.](./images/status_bar.png "Status bar")

2. After logging in, you should see the main page with a series of
   buttons at the top:

    ![Core functions.](./images/menu.png "Menu buttons")

    - `Sync Setup`: Specify the destination of the data transfer and prepare
    the sync.
    - `Report Issue`: Send an email to the support team, attaching the log file stored in your home directory.
    - `About`: Open the [OMI-DB home page](https://medphys.royalsurrey.nhs.uk/omidb/),
       where useful documentation about the database can be found.

3. The text box displaying the default download directory can be edited
   manually, or graphically by clicking the `Select` button. If the specified
   directory does not exist, it will be created when starting a new
   sync.

    ![Configurable download directory.](./images/dir_selector.png "Input the download directory")

4. With the download directory set, click the `Start Sync` button. Before the
   download starts, a prompt will appear showing the disk space required to
   complete the database. **Note that the download is destructive, i.e.
   existing files may be overwritten or deleted**. You will be asked if you would
   like to include or skip any existing files found on your local machine:

    - `Include`: The tool will run over the entire database, checking the status
       of any files you have already downloaded. This is useful when you want
       to check for updates or recover synable files. For example, missing JSON
       files will be downloaded again, but missing DICOM files will not.

    - `Skip`: Only new files (those that have not previously been accessed) will be
      transferred. If there are no new files, no sync will take place when
      selecting this option. **We recommend selecting this option when running
      the tool on an incomplete database, i.e. during the initial transfer.**

    ![Sync options.](./images/download_prompt.png "Sync options")

5. _Syncing for the first time_. The download will now start even if the destination does not have sufficient space to accommodate the database. The status bar will display the current
   resource number  (typically client/study folders) and total progress, along
   with the download start time. You should also see the following option:

    ![Stopping the sync.](./images/stop_sync.png "Stopping the sync")

6. Clicking `Stop Sync` will stop the transfer as soon as the current
   resource has been successfully processed.

7. _Running_ `Start Sync` _a second time_. Running another sync will continue
   to download any resources (images and data folders) that were not captured
   in a previous session, and, if `Included` was selected, check for updates on
   any existing metadata _(not images)_.


\pagebreak

## Resources

- If you have any issues, would like to report a bug or suggest improvements to
our software, please contact [rsch.scicom@nhs.net](mailto:rsch.scicom@nhs.net?subject=OMI-DB Sync Tool), with the subject heading "OMI-DB Sync Tool".

- A Python API to the OMI-DB database can be found at [https://scicomcore.bitbucket.io/omidb](https://scicomcore.bitbucket.io/omidb/)

- Visit [https://medphys.royalsurrey.nhs.uk/omidb](https://medphys.royalsurrey.nhs.uk/omidb/) for general information, including database statistics, on OMI-DB.
