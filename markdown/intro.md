# Introduction

The OPTIMAM Medical Image Database (OMI-DB) [collects NHS Breast Screening Programme images](https://medphys.royalsurrey.nhs.uk/omidb/about-prospects/collection/)
from multiple breast screening centres across the UK, with the goal of serving
as a large repository of medical images for research and training purposes.
OMI-DB has a mandate to share its data and has been designed for the use of the
entire research community: research groups/institutions can [apply for
access](https://medphys.royalsurrey.nhs.uk/omidb/getting-access/) to the
images.

This document details the file structure of the database shared with
third-parties, explains the nomenclature describing the hierarchical nature of
the breast screening data, and describes metadata properties and relational
aspects of NBSS (National Breast Screening System) and ImageDB (Image
Database). The final section provides download links and user instructions for
The OMI-DB Sync Tool, a software application for distributing the image
database.
