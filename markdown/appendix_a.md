\pagebreak

# Appendix A: Film Reader Opinion

The ScreeningProcedure table listed on page 84 of the \href{https://bitbucket.org/scicomcore/omi-db-sharing-guide/raw/d9e023e39760b12decec67f6407beee91cb82ac2/pdfs/crystal_nbss_guide.pdf}{NBSS User Guide} provides screening film side-dependent information. This table contains opinions for readers one to five, stored as SIRE/SIRO-formatted codes under the property `FilmReader<n>Opinion`, where `n` is the reader. The following descriptions are provided to aid the reader interpret the meaning of these five codes, based on the comments of two radiologists:

\begin{table}[h!]
\begin{center}
\begin{tabularx}{\linewidth}{l l X}
\bf Opinion Code & \bf Opinion & \bf Description \\ \hline \hline
\textbf{A} & Abnormal & Radiology recalls the patient for assessment. \\ \hline
\textbf{C} & Clinical & The client has symptoms or a radiographer notices a change at the time of taking mammogram. It is a symptomatic recall from screening mammogram \\ \hline
\textbf{N} & Normal & Radiologically normal. \\ \hline
\textbf{T} & Technical Recall & Radiology recalls the patient for another screening due to technical issues. \\ \hline
\textbf{X} & Not Applicable & Mammogram has not been performed. It may be that the patient has a mastectomy, or the patient declines the procedure half way through. \\
\end{tabularx}
\end{center}
\end{table}
