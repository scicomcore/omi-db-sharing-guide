#!/usr/bin/env xonsh
import webbrowser
import argparse

parser = argparse.ArgumentParser()

parser.add_argument('--noshow', action='store_true')

args = parser.parse_args()

version = $(find . -name '__version__.txt' | xargs cat).strip()

# Prep
input_file = ['markdown/intro.md', 'markdown/structure.md',
              'markdown/database_jsons.md', 'markdown/sync_tool.md',
              'markdown/appendix_a.md']

output_file = f'./pdfs/OMI-DB-Sharing-Guide_v{version}.pdf'
metadata = 'meta.yaml'
style = 'tango'

# Execute the build command
print('Building document')
pandoc @(input_file) --highlight-style=tango -o @(output_file) @(metadata) --toc -s

if not args.noshow:
  webbrowser.open_new(output_file)
