version = $(find . -name '__version__.txt' | xargs cat).strip()
git tag @(version)
git push origin @(version)
