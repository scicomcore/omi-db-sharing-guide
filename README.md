 OMI-DB Share Guide

## What?

- `README.md`: This
- `./markdown`: The main document content
- `meta.yaml`: Title, author, data, plus latex specifics
- `make.xsh`: See below
- `./pdfs`: PDF builds and a copy of the NBSS User Guide

## Build

To build the user guide as a PDF, you will need to install [Pandoc](https://pandoc.org/) and [latex](https://www.latex-project.org/get/).

`make.xsh` is a [xonsh](https://xon.sh/index.html)-shell script to automate the Pandoc build:

```
xonsh make.xsh
```
